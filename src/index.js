// The "import" statement allows us to use the code/exported modules from other files similar to how we use the "require" function in Node JS.

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


//ReactDOM is a package that provides DOM specific methods than can be used at the top level of a web app to enable an efficient way of managing DOM elements of the web page.
      //.createRoot controls the content of the container node you pass in. Any existing DOM elements inside are replaced when render is called.

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // React.StrictMode is a built-in react component which is used to highlight potential problems in our code and in fact allows for more information about errors in our code.
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*
  The syntax used in Reactjs is JSX
  JSX - JavaScript + XML
    - It is an extension of Javscript that let's us create objects which will then be compiled and added as HTML elements. 
    - We are able to create HTML elements using JS

*/

// const name = "John Smith";

// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(user){
//   return user.firstName + " " + user.lastName; 
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// // root.render() - allows to render/display our reactjs elements in our HTML file.
// root.render(element);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals


