import { useState, useEffect, useContext } from "react";
import {Link, useParams, useNavigate } from "react-router-dom";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function CourseView(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const { courseId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (courseId) => {
		if (user.id === null) {
			navigate("/login");
			return;
		}
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
        .then(res => res.json())
        .then(data => {
            if (data === true) {
            	Swal.fire({
            		title: "Successfully enrolled!",
            		icon: "success",
            		text: "You have successfully for this course."
            	})
            	navigate("/courses");
            }
            else {
            	Swal.fire({
            		title: "Something went wrong!",
            		icon: "error",
            		text: "Please try again later."
            	})
            }
        })
	}

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(res => res.json())
        .then(data=>{
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
	}, []);
	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body>
							<Card.Title className="mb-3">{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>
							<div className="d-grid">
							{
								(user.id !== null)?
								<Button onClick={()=>enroll(courseId)} variant="primary"  size="lg">Enroll</Button>
								:
								<Button as ={Link} to="/login"variant="primary"  size="lg">Login to Enroll</Button>
							}
							</div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}