import {useContext, useEffect} from "react";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";

export default function Logout(){
const {user, setUser} = useContext(UserContext);
	const unsetUser = () =>{
    localStorage.clear();
  }

	useEffect(()=>{
		unsetUser();
		setUser({
			id: null,
			isAdmin: null
		})
	}, [user]);

	return(
		//Redirect back to the login
		<Navigate to="/login" />
	)
}
